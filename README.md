# Django Project Template


[![pipeline status](https://gitlab.com/mastour-anas/django-project-template/badges/master/pipeline.svg)](https://gitlab.com/mastour-anas/django-project-template/commits/master)



## Features

This Django project template sets up a new project with the following features:

* PostgreSQL for database connections.
* Sensible time zone and defaults for Brisbane, Australia.
  * Note that internationalization is disabled!
* `settings` app to store site-specific settings by providing `settings/dev.py` and `settings/prod.py`
  files.
* Enables the [django-debug-toolbar](https://github.com/jazzband/django-debug-toolbar) app for development
* Serves static and media files when using the development server.
* Password validation is enabled for Django's auth system.


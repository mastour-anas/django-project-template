"""
Django production settings for {{ project_name }} project.
"""

from .base import *

DEBUG = False

# Hosts/domain names that are valid for this site.
# See https://docs.djangoproject.com/en/1.10/ref/settings/#allowed-hosts
ALLOWED_HOSTS = []

ADMINS = [
    # ('Your Name', 'your_email@example.com'),
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = '{{ secret_key }}'

# Database settings. We define this here so it can be used for both dev and prod
# configurations. Add HOST and PORT if the database is not local.
# See https://docs.djangoproject.com/en/1.10/ref/settings/#databases for details.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
    }
}

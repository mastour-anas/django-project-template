"""
Django development settings for {{ project_name }} project.
"""

from .base import *

DEBUG = True

INTERNAL_IPS = ['127.0.0.1']

ADMINS = [
    # ('Your Name', 'your_email@example.com'),
]

# Make this unique, and don't share it with anybody.
SECRET_KEY = '{{ secret_key }}'

# Database settings. We define this here so it can be used for both dev and prod
# configurations. Add HOST and PORT if the database is not local.
# See https://docs.djangoproject.com/en/1.10/ref/settings/#databases for details.
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}


# Add django-debug-toolbar
INSTALLED_APPS.append('debug_toolbar')
MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')
DEBUG_TOOLBAR_PATCH_SETTINGS = False
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

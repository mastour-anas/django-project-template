"""{{ project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from django.conf import settings
from django.conf.urls.static import static
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()


urlpatterns = []

INSTALLED_APPS = getattr(settings, "INSTALLED_APPS")
for app in INSTALLED_APPS:
    test = "{}.urls".format(app)
    try:
        urlpatterns += [url(r'^', include(test)), ]
    except:
        pass

urlpatterns += [
    # Examples:
    # url(r'^$', {{ project_name }}.views.home, name='home'),
    # url(r'^{{ project_name }}/', include('{{ project_name }}.{{ project_name }}.urls')),


    #url(r'^', include('django.contrib.auth.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]

if settings.DEBUG:
    if 'debug_toolbar' in settings.INSTALLED_APPS:
        urlpatterns.append(
            url('__debug__/', include('debug_toolbar.toolbar', namespace='djdt')),
        )
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
